:: Compile and run desktop Java OpenGL app
:: Using lwjgl 2.9.3

echo off

:: Guess the default JDK installation location
set JDK_HOME="C:\Program Files\Java\jdk1.7.0_79"
set JDK_BIN=%JDK_HOME%\bin
set JAVAC=%JDK_BIN%\javac
set JAVA=%JDK_BIN%\java

set MAINCLASS=QuadExample
%JAVAC% %MAINCLASS%.java -classpath ".;jar/*"
%JAVAC% QuadDraw.java -classpath ".;jar/*"
%JAVA% -cp ".;jar/*" %MAINCLASS% -Djava.library.path="native/windows/"
