import org.lwjgl.opengl.GL11;
  
public class QuadDraw {
    public void initGL() {}
    public void exitGL() {}
    public void display() {

        // set the color of the quad (R,G,B,A)
        GL11.glColor3f(0.5f,0.5f,1.0f);
             
        // draw quad
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glVertex2f(10,100);
        GL11.glVertex2f(100+200,100);
        GL11.glVertex2f(100+200,100+200);
        GL11.glVertex2f(100,100+200);
        GL11.glEnd();

    }
    public void timestep(double absTime, double dt) {}
}
