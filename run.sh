#!/bin/bash
# Compile and run desktop Java OpenGL app
# Using lwjgl 2.9.3

javac QuadExample.java -cp .:jar/*

if [ `uname` == "Linux" ]; then
    java -cp .:jar/* -Djava.library.path='native/linux/' QuadExample
else
    java -cp .:jar/* -Djava.library.path='native/macosx/' QuadExample
fi

