# OpenGL with Java #

## Description ##
A simple testbed to get started in OpenGL programming using java.

### Setup ###

#### Windows 8 ####

 - Install the JDK fron [Oracle](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
 - OpenJDK is also an option
 - `java` will be added to your `%PATH%` by default, `javac` will not.

### Usage ###

```
"c:\Program Files (x86)\Java\jdk1.7.0_45\bin\javac.exe" HelloWorld.java -classpath ".;lib/*"
java -cp ".;lib/*" HelloWorld
```
